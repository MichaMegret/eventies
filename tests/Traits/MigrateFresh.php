<?php
 
namespace Tests\Traits;
 
use Illuminate\Support\Facades\Artisan;
 
trait MigrateFresh{
 
    protected function setUp(): void
    {
        parent::setUp();
 
        Artisan::call('migrate:fresh');
    }
    
}