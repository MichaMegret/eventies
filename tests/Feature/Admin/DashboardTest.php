<?php

namespace Tests\Feature\Admin;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class DashboardTest extends TestCase
{
    /**
     * Test display of admin dashboard where user is connected
     */
    public function test_dashboard_can_be_rendered_when_user_is_admin(): void
    {
        $user = User::factory()->create();
        $user->role = "admin";
        Auth::login($user);
        $response = $this->get('admin/dashboard');

        $response->assertStatus(200);
    }

    /**
     * Test display of admin dashboard where user is connected
     */
    public function test_dashboard_can_not_be_rendered_when_user_is_not_admin(): void
    {
        $user = User::factory()->create();
        Auth::login($user);
        $response = $this->get('admin/dashboard');

        $response->assertRedirect("/");
    }
}
