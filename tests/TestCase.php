<?php

namespace Tests;

use Tests\Traits\MigrateFresh;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication,MigrateFresh;
}
