@section('title')
    {{ 'Liste des événements' }}
@endsection

@push('styles')
    <link rel="stylesheet" href="/assets/css/list.css">
@endpush

@push('styles')
    <link rel="stylesheet" href="/assets/css/list-event.css">
@endpush

<x-app-layout>

    <h1 class="list-title">
        événements
    </h1>

    <div class="table-option">
        <a class="create-event-button" href="{{route('admin.event.create')}}">
            Nouvel événement
        </a>
    </div>


    <div class="list-container">

        <div class="table-container">

            <table id="birds-table">
            
                <thead class="">
                    <tr class="">
                        <th class="">Nom</th>
                        <th class="">Localisation</th>
                        <th class="">Participants</th>
                        <th class="">Etat</th>
                    </tr>
                </thead>
        
        
                <tbody>
        
                    @if (count($events)>0)
                        @foreach ($events as $event)
        
                        <tr class="">
                            <td>
                                <a href="update-event/{{ $event->id }}" class="">
                                    <div class="event-title">
                                        <span class="">{{ $event->title }}</span>
                                    </div>
                                </a>
                            </td>
                            <td>
                                <a href="update-event/{{ $event->id }}" class="">
                                    <div class="event-location">
                                        <span class=" ">{{ $event->location }}</span>
                                    </div>
                                </a>
                            </td>
                            <td>
                                <a href="update-event/{{ $event->id }}" class="">
                                    <div class="event-attendees">
                                        <span class="">0 / {{ $event->max_attendees }}</span>
                                    </div>
                                </a>
                            </td>
                            <td>
                                <a href="update-event/{{ $event->id }}" class="">
                                    <div class="event-state">
                                        @if ($event->start_time < date("Y-m-d") && $event->end_time > date("Y-m-d"))
                                            <span class="event-state progress">En cours</span>
                                        @else
                                            @if ($event->end_time < date("Y-m-d"))
                                            <span class="event-state terminate">Terminé</span>
                                            @else
                                            <span class="event-state coming">A venir</span>
                                            @endif
                                        @endif
                                    </div>
                                </a>
                            </td>
                        </tr>
        
                        @endforeach                
                    @else
                        <tr>
                            <td colspan="5" class="">
                                <span class="">No event found</span>
                            </td>
                        </tr>
                    @endif
        
                </tbody>
        
            </table>
        </div>
        
        {{ $events->links('pagination.custom-pagination') }}

    </div>


</x-app-layout>