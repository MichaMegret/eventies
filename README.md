1 - git clone https://gitlab.com/MichaMegret/eventies.git

2 - cd eventies

3 - composer install

4 - cp .env.example .env

5 - php artisan key:generate

6 - php artisan migrate

7 - php artisan db:seed

8 - npm install

9 - npm run -dev

10 - php artisan storage:link

11 - php artisan serve